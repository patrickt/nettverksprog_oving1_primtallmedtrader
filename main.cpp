#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <cmath>
#define CATCH_CONFIG_MAIN
#include "catch.hpp"


using namespace std;
vector<int> primes;

bool isPrime(int x) {
    if(x<=1){
        return false;
    }
    else if(x<=3){
        return true;
    }
    else if(x%2 == 0){
        return false;
    }
    else {
        for (int i = 5; i < x/2; i += 2) {
            if (x % i == 0)
                return false;
        }
    }
    return true;
}

void finnPrimtall(int from, int to) {


    for (int i = from; i <= to; i++) {
        if (isPrime(i)) {
            primes.emplace_back(i);
        }
    }
}
void skrivUtPrimtall(){

    for (int i = 0; i<(int)primes.size(); i++){
        cout << primes.at(i) << endl;
    }
}

void finnPrimtallMedTrader(int from, int to, int amt) {
    vector<thread> threads;
    mutex primes_mutex;
    int length = to - from;
    int split = length / amt;
    int splitCurr = split;
    int fromVar = from;

    for (int i = 0; i < amt; ++i) {
        threads.emplace_back(thread([&] {
            lock_guard<mutex> lock(primes_mutex);
            finnPrimtall(fromVar, splitCurr);
            fromVar = splitCurr+1;
            splitCurr += split;
        }));
    }
    for (int i = 0; i < (int)threads.size(); i++) {
        threads.at(i).join();
    }

    skrivUtPrimtall();
}
/*
 int main(){
    finnPrimtallMedTrader(0, 1000000, 2);

}*/

TEST_CASE("Test 1-20", "[primtall]")
{
    REQUIRE(isPrime(0) == false);
    REQUIRE(isPrime(1) == false);
    REQUIRE(isPrime(2) == true);
    REQUIRE(isPrime(3) == true);
    REQUIRE(isPrime(4) == false);
    REQUIRE(isPrime(5) == true);
    REQUIRE(isPrime(6) == false);
    REQUIRE(isPrime(7) == true);
    REQUIRE(isPrime(8) == false);
    REQUIRE(isPrime(9) == true);
    REQUIRE(isPrime(10) == false);
    REQUIRE(isPrime(11) == true);
    REQUIRE(isPrime(12) == false);
    REQUIRE(isPrime(13) == true);
    REQUIRE(isPrime(14) == false);
    REQUIRE(isPrime(15) == false);
    REQUIRE(isPrime(16) == false);
    REQUIRE(isPrime(17) == true);
    REQUIRE(isPrime(18) == false);
    REQUIRE(isPrime(19) == true);
    REQUIRE(isPrime(20) == false);
}